
package com.example.osfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.Random;
import java.util.*;

public class MainActivity extends AppCompatActivity {

    String a, b, c, e, randNum;
    int low, low2, high, high2;
    Boolean always = true;
    static Boolean isWeak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText input;
        Button result;
        TextView display;
        Button refresh;
        Button newactivity;
        Button weak;
        Button strong;

        input = (EditText)findViewById(R.id.Input);
        result = (Button) findViewById(R.id.Result);
        display = (TextView) findViewById(R.id.Display);
        refresh = (Button) findViewById(R.id.Refresh);
        newactivity = (Button) findViewById(R.id.Saved);
        weak = (Button) findViewById(R.id.Weak);
        strong = (Button) findViewById(R.id.Strong);

        strong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (always = true) {
                 isWeak = false;
                 display.setText(isWeak.toString());
                }
            }
        });

        weak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //display.setText("weak");
                if (always = true){
                    isWeak = true;
                    display.setText(isWeak.toString());
                }
            }
        });

        result.setOnClickListener(new View.OnClickListener()
        {
           public void onClick(View v)
           {
               Random r = new Random();
               low = 1000;
               high = 2000;
               int rand = r.nextInt(high - low) + low;

               Random d = new Random();
               low2 = 10;
               high2 = 20;
               int rand2 = d.nextInt(high2 - low2) + low2;

               final String[] chars = {"!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_"};
               //11 total random characters

               Random m = new Random();
               int index = m.nextInt(chars.length);
               a = chars[index];

             //  Random n = new Random();
               int index2 = m.nextInt(chars.length);
               b = chars[index2];

              // Random o = new Random();
               int index3 = m.nextInt(chars.length);
               c = chars[index3];

               int index4 = m.nextInt(chars.length);
               e = chars[index4];

               randNum = Integer.toString(rand);

             //  display.setText(isWeak.toString() + "static?");
               //display.setText(input.getText().toString() + a + randNum + b + c);

               //just checking boolean values doesn't work? But comparing toString values does work for some reason?
               if (isWeak.toString() == "true") {
                 // display.setText("weak pressed");
                   display.setText(a + input.getText().toString() + rand2);
               }
              if (isWeak.toString() == "false"){
                  // display.setText("strong pressed");
                  display.setText(a + rand + input.getText().toString() + b + c + rand2 + e);
              }
            //  else {
            //      display.setText("idk how boolean got third option but wow");
           //   }
           }

        }
        );

         refresh.setOnClickListener(new View.OnClickListener()
         {
             public void onClick(View v)
             {
                 display.setText(" ");
             }
         }
         );

         newactivity.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(MainActivity.this, Activity2.class);
                 startActivity(intent);
             }
         });
    }
}