package com.example.osfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);


        Button goBack;
        FloatingActionButton add;
        goBack = (Button) findViewById(R.id.back);
        add = (FloatingActionButton) findViewById(R.id.add);

       Button Item1;
       EditText enter1;
       EditText Item2;
       EditText enter2;


        enter1 = (EditText)findViewById(R.id.Enter1);
        Item2 = (EditText)findViewById(R.id.Item2);
        enter2 = (EditText)findViewById(R.id.Enter2);


        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity2.this, MainActivity.class);
                startActivity(intent);

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Item2.setVisibility(View.VISIBLE);
                enter2.setVisibility(View.VISIBLE);

            }
        });


    }


}